package com.category.app;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

import com.category.app.dao.CategoryRepoitory;
import com.category.app.dao.IProduct;
import com.category.app.dao.SubCategoryRepository;
import com.category.app.entities.Category;
import com.category.app.entities.SubCategory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceApplicationTests {
   
	@Autowired
	private CategoryRepoitory categoryRepository;
	@Autowired
	private SubCategoryRepository subCategoryRepository;
	@Autowired
	private IProduct iProduct;
	@Autowired private MongoOperations mongoOps;
	
	@Test
	public void shouldSaveCategory() {
		//create new category object
		Category category = new Category("5c8b6c643cdea2000128b399","informatique");
		//persist the object with the repository
		Category newcategory	= categoryRepository.save(category);
		//Delete TestData
		mongoOps.remove(newcategory);
		//check if the category is created
		assertNotNull("category is not created", newcategory);
		//check if the category is created with the correct  name
		assertEquals("the name is incorrect",newcategory.getName(), "informatique");
		

	}
	@Test
	public void shouldGgetCategories(){
		//persist 2 categories using mongoOperations
		Category category1 =mongoOps.save(new Category("5c8b6c643cdea2000128b399","informatique"));
		Category category2 =mongoOps.save(new Category("5c8b6c643cdea2000128b400","electromenagers"));

		// get the categories list using the repository
		List<Category> categories = categoryRepository.findAll();
		//Delete Test Data
		mongoOps.remove(category1);
		mongoOps.remove(category2);		
		//check if the list is not null
		assertNotNull("the list is empty", categories);


				
	}
	@Test
	public void shouldSaveSubCategory() {
		SubCategory subcategory = new SubCategory("5c88c6d0b9d82100015cbed2","smartphone","5c8b6c643cdea2000128b399");
		//persist the subCategory object with the subCategory repository
		SubCategory newsubcategory	= subCategoryRepository.save(subcategory);
		//Delete TestData
		mongoOps.remove(newsubcategory);
		//check if the subcategory is created
		assertNotNull("subcategory is not created", newsubcategory);
		//check if the subcategory is created with the correct  name
		assertEquals("the name is incorrect",newsubcategory.getName(), "smartphone");
		//check if the subcategory is created with the correct  category
		assertEquals("the category is incorrect",newsubcategory.getCategory(),"5c8b6c643cdea2000128b399");
		


	}
	@Test
	public void shouldGetOneSubCategory(){
		//persist  subcategory using mongoOperations
		mongoOps.save(new SubCategory("5c88c6d0b9d82100015cbed2","smartphone","5c8b6c643cdea2000128b399"));
		//Get subcategory by id
		Optional<SubCategory> subcategory= subCategoryRepository.findById("5c88c6d0b9d82100015cbed2");
		//check if the subcategory is fetched
		assertNotNull("subcategory is not created", subcategory);
		//Get Products
		List<Object> products = iProduct.getProducts("5c88c6d0b9d82100015cbed2");
		//Delete TestData
		mongoOps.remove(subcategory.get());
		//check if the list of products is not null
		assertNotNull("the list is empty", products);
		subcategory.get().setProducts(products);
		//check if size of the list is correct
		Assert.assertThat("the size is incorrect",subcategory.get().getProducts().size(),is(1));
		iProduct.deleteProducts("5c88c6d0b9d82100015cbed2");

		
	}
	@Test
	public void shouldGetsubCategories(){
		//persist 2 categories using mongoOperations
		SubCategory subcategory1 = mongoOps.save( new SubCategory("5c88c6d0b9d82100015cbed2","smartphone","5c8b6c643cdea2000128b399"));
		SubCategory subcategory2 = mongoOps.save( new SubCategory("5c88c6d0b9d82100015cbed3","Ref","5c8b6c643cdea2000128b400"));

		// get the categories list using the repository
		List<SubCategory> subcategories = subCategoryRepository.findAll();
		//Delete Test Data
		mongoOps.remove(subcategory1);
		mongoOps.remove(subcategory2);
		//check if the list is not null
		assertNotNull("the list is empty", subcategories);
		

		
	}
	@Test
	public void shouldGetsubCategoriesByCategory(){
		Category category1 =mongoOps.save(new Category("5c8b6c643cdea2000128b399","informatique"));
		//persist 2 categories using mongoOperations
		SubCategory subcategory1 = mongoOps.save( new SubCategory("5c88c6d0b9d82100015cbed2","smartphone","5c8b6c643cdea2000128b399"));
		SubCategory subcategory2 = mongoOps.save( new SubCategory("5c88c6d0b9d82100015cbed3","ordinateurs","5c8b6c643cdea2000128b399"));

		// get the categories list using the repository
		List<SubCategory> subcategories = subCategoryRepository.findByCategory("5c8b6c643cdea2000128b399");
		//Delete Test Data
		mongoOps.remove(category1);
		mongoOps.remove(subcategory1);
		mongoOps.remove(subcategory2);
		//check if the list is not null
		assertNotNull("the list is empty", subcategories);
		//check if size of the list is correct
		Assert.assertThat("the size is incorrect",subcategories.size(),is(2));

		
	}
}
