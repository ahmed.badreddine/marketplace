FROM openjdk:8
VOLUME /tmp
ADD wait-for-it.sh .
RUN ["chmod", "+x", "wait-for-it.sh"]
ADD CategoryService/target/CategoryService-0.0.1-SNAPSHOT.jar app.jar 
EXPOSE 8902
ENTRYPOINT ["./wait-for-it.sh", "product-service:8901", "--","java","-jar","app.jar"] 
